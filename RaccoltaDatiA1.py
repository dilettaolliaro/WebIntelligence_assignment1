import urllib2
import bs4


#dictionary having key: name of month, value: number of days in that month. We assume february with 28 days for practical reasons
#in fact we need to take only 1000 urls and it won't even arrive to february
months = {"gen": 31, "feb": 28, "mar": 30, "apr": 30, "may": 31, "jun": 30, "jul": 31, "aug": 31, "sep": 30, "oct": 31,
          "nov": 30, "dec": 31}
#categories in which we cycle to take news from different topics
categories = ["sport", "culture", "business", "lifeandstyle", "technology", "environment"]
#general structure of "The Guardian"'s urls
URL = "https://www.theguardian.com/uk/%s/2017/%s/%s"


def create_list(links):
    url_list = []
    for link in links:
        #Checking if a particular url isn't already present in our list, if it isn't we add it
        if link["href"] not in url_list:
            url_list.append(link["href"])
    return url_list

#After the analysis of the HTML page we were able to find the urls using the library BeautifulSoup
def get_urls(url):
    print("getting:%s" % url)
    urls_list = []
    try:
        u = urllib2.urlopen(url).read()
        pages = bs4.BeautifulSoup(u, "html.parser")
        divs = pages.find_all('div', class_='fc-slice-wrapper')
        for div in divs:
            links = div.find_all('a')
            urls_list.extend(create_list(links))
        return urls_list
    except urllib2.HTTPError:
        return []


def build_urls(n):
    articles_list = []
    #we cycle just on three months because it is sufficient to reach the goal of 1000 urls
    for m in ["oct", "sep", "aug"]:
        days = months[m]
        for d in range(days, 0, -1):
            if d<10:
                day = "0"+str(d)
            else:
                day = str(d)
            for c in categories:
                articles_list.extend(get_urls(URL % (c, m, day)))
            #At the end of one day we check the number of urls we got until this moment, if it's bigger then 1000 we return the list containing the strings representing the urls
            if len(articles_list) > n:
                return articles_list


if __name__ == "__main__":
    #opening file "urls.txt" to write in there the strings containg every url
    with open("urls.txt", "w") as u:
        urls = build_urls(1000)
        #we write the different urls in a text file, ordering them so that later we can create an omogeneous user
        for url in sorted(urls):
            u.write(url+"\n")
