import urllib2
import bs4
import hashlib
import os
import time
import io


# Thanks to the analisys of the HTML page and the libriry BeautifulSoup we were able to extract from the whole HTML
# text with tags and other non-significant elements title and body of the news
def make_file(u, code):
    page = bs4.BeautifulSoup(u, "html.parser")
    title = page.find('h1', class_="content__headline").text
    divs = page.find_all('div', class_="content__article-body")
    article = title + u"\n"
    for b in divs:
        ps = b.find_all('p')
        for p in ps:
            article = article + p.text
    with io.open(os.path.join("news", code), "w", encoding="utf8") as buf:
        buf.write(article)

#we check if we've already download a precise news so that if we've already done it we won't do it again
def check(code):
    path = os.path.join("cache", code)
    if os.path.isfile(path):
        return True
    else:
        return False


def main():
    urls = []
    with open("urls.txt", "r") as u:
        for line in u:
            urls.append(line.strip())
    for url in urls:
        #we code the url to use it as name of the file that will contain the related news
        #we don't use the url itself because it contains special character that are not allowed as file name
        code = hashlib.md5(b"%s" % url).hexdigest()
        print("getting: "+url, code)
        #if we don't have already the news in our "cache" we download it
        if not check(code):
            time.sleep(0.1)
            u = urllib2.urlopen(url).read()
            with io.open(os.path.join("cache", code), "w", encoding="utf8") as new:
                new.write(u.decode("utf8"))
        #if we already have it we're just going to read it again
        else:
            with io.open(os.path.join("cache", code), "r", encoding="utf8") as existing:
                u = existing.read()
        #either case we call this function to extract the significant parts from the text
        make_file(u, code)


if __name__ == "__main__":
    main()
