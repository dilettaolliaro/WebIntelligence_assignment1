import RaccomandazioniB1
from gensim.utils import lemmatize


# we lemmatize the remaining words one by one so that we won't lose anyone of them
# (when lemmatize() doesn't comprehend the right structure of a sentence sometimes it won't consider
# some repetitions, not considering them relevant lemmas
def lem(freq):
    # we create an empty dictionary that will contain every lemma as a key and the related frequence as a value
    lemfreq = {}
    for key, value in freq.iteritems():
        li = lemmatize(key)
        if li:
            if li[0] not in lemfreq:
                lemfreq[li[0]] = 0
            lemfreq[li[0]] += value
    # we sort our list of lems with respect to the frequence
    sortedlemfreq = sorted(lemfreq.iteritems(), key=lambda (k, v): v, reverse=True)
    # then we save rank, word and related frequence of the first 500 most frequent words on a file

    with open("b2.txt", "w") as f:
        for i in range(500):
            f.write("%s %s %s\n" % ((i + 1), sortedlemfreq[i][1], sortedlemfreq[i][0]))
    return lemfreq


# we delete non-significant words
# we could use a parameter of lemmatize to do that but we prefered to follow the order on the problem specific
# and do it by ourselves so we're sure that before using lemmatize we deleted the non-significant words
def delete_useless(freq, useless=None):
    if useless is None:
        useless = []
        with open("stopwords-en.txt", "r") as f:
            for word in f:
                useless.append(word.strip())
    for word in useless:
        if word in freq:
            del freq[word]
    return freq


if __name__ == '__main__':
    res = delete_useless(RaccomandazioniB1.analyze("urls.txt"))
    lem(res)
