import RaccomandazioniB3
import gensim
import io


# we create lsi model based on the bows created on our 1000 and more documents
# we also use the "id2word2" parameter so that later we will obtain understandable outputs
def do_lsi(bows, k, lexicon):
    lsi = gensim.models.LsiModel(bows, id2word=lexicon, num_topics=k)
    return lsi


def main(n):
    listoflems, listofdocs = RaccomandazioniB3.create_lems("urls.txt")
    lexicon = RaccomandazioniB3.create_lexicon(listoflems)
    all_bows = RaccomandazioniB3.create_bow(lexicon, listoflems)
    user_lems = listoflems[:n]
    user = RaccomandazioniB3.create_user(user_lems, lexicon)
    tfidf = RaccomandazioniB3.do_tfidf(all_bows)
    with io.open("HomoUser.txt", "w", encoding="utf8") as f:
        for doc_id in range(n):
            f.write("Doc_id = %d\n%s\n" % (doc_id, listofdocs[doc_id]))
    k = [50, 100, 200]
    with io.open("LsiHomoUserResult.txt", "w", encoding="utf8") as f:
        for i in k:
            lsi = do_lsi(tfidf[all_bows], i, lexicon)
            result = RaccomandazioniB3.do_cossim(lsi, lsi[user], all_bows)
            f.write(u"LSI Model for k = %s"% str(i))
            for doc_id, sim in result[:5]:
                f.write(u"ID = %d, Similarity = %f \n %s \n" % (doc_id, sim, listofdocs[doc_id].strip().split("\n")[0]))
            f.write(u"Topics: %s\n" % lsi.print_topics(num_topics=k))

if __name__ == '__main__':
    main(20)
