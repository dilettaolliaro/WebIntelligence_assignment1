import hashlib
import os
import io
import re
import gensim


def create_lems(urlsfile):
    urls = []
    # we create a list containing all the urls contained in the input file
    with open(urlsfile, "r") as f:
        for line in f:
            urls.append(line.strip())
    useless = []
    # we create a list containing all the useless words contained in the file "stopwords-en.txt"
    with open("stopwords-en.txt", "r") as f:
        for word in f:
            useless.append(word.strip())
    listoflems = []
    listofdocs = []
    for url in urls:
        # we encode the title so that we can get the related document
        filename = hashlib.md5(b"%s" % url).hexdigest()
        with io.open(os.path.join("news", filename), "r", encoding="utf8") as news:
            line = news.read()
            # for every word we apply lemmatize() and we create a unique list, using the parameters "stopwords" that
            # will delete from the result the words contained in the list "useless"
            lems = []
            for word in re.split("[^a-z']+", line.lower()):
                lems.extend(gensim.utils.lemmatize(word, stopwords=useless))
            listofdocs.append(line.strip())
            listoflems.append(lems)
    return listoflems, listofdocs


# we create our lexicon
def create_lexicon(listoflems):
    lexicon = gensim.corpora.Dictionary(listoflems)
    lexicon.filter_extremes(no_below=2)
    return lexicon


# we represent our list of lems with the bag of words representation"
def create_bow(lexicon, listoflems):
    bows = [lexicon.doc2bow(n) for n in listoflems]
    return bows


# we create tfidf model based on the bows created on our 1000 and more documents
def do_tfidf(bows):
    tfidf = gensim.models.TfidfModel(bows)
    return tfidf


# this function implements the cosine similarity, it takes the model we will use, the profile user (20 chosen
# documents) and the bows created on the 1000 and more documents
def do_cossim(model, user, all_bows):
    sim = [(doc_id, gensim.matutils.cossim(user, model[all_bows[doc_id]])) for doc_id in range(len(all_bows))]
    return sorted(sim, key=lambda (doc_id, score): score, reverse=True)


# we create a "fake" profile user taking the first 20 documents from the ones we downloaded in the first place we
# merge those 20 docs to create the "user", first we merge the 20 list of lems created from the original docs,
# secondly we create apply the mean as a matter of fact after summing the 20 docs (actually merging their list of
# lems we divide every lem frequence by 20, and finally we create a unique bag of words
def create_user(listoflems, lexicon):
    user = []
    for i in range(len(listoflems)):
        user.extend(listoflems[i])
    user_bow = lexicon.doc2bow(user)
    user = map(lambda (x, y): (x, (y / len(listoflems))), user_bow)
    return user


def main(n):
    listoflems, listofdocs = create_lems("urls.txt")
    lexicon = create_lexicon(listoflems)
    all_bows = create_bow(lexicon, listoflems)
    user_lems = listoflems[:n]
    user = create_user(user_lems, lexicon)
    tfidf = do_tfidf(all_bows)
    result = do_cossim(tfidf, tfidf[user], all_bows)
    with io.open("HomoUser.txt", "w", encoding="utf8") as f:
        for doc_id in range(n):
            f.write("Doc_id = %d\n%s\n" % (doc_id, listofdocs[doc_id]))
    with io.open("TfidfHomoUserResult.txt", "w", encoding="utf8") as f:
        for doc_id, sim in result[:10]:
            f.write("Doc_id = %d, Similarity = %f\n %s\n" % (doc_id, sim, listofdocs[doc_id].strip().split("\n")[0]))


if __name__ == '__main__':
    main(20)
