import io
import hashlib
import os
import re


def analyze(file):
    urls = []
    # we take every url and we put it in a list so we can open the related file later
    with open(file, "r") as f:
        for line in f:
            urls.append(line.strip())
    # we create an empty dictionary that will contain every word as a key and the related frequence as a value
    freq = {}
    # for every document we check every word adding it to our dictionary if it wasn't already added,
    # in the opposite case we increase its frequence
    for url in urls:
        filename = hashlib.md5(b"%s" % url).hexdigest()
        with io.open(os.path.join("news", filename), "r", encoding="utf8") as news:
            for line in news:
                # with the declared regexp we avoid every character that is not included
                # between lower 'a' and 'z' and the apostrophe; we use line.lower() not to have capital letters
                for word in re.split("[^a-z']+", line.lower()):
                    if word:
                        if word not in freq:
                            freq[word] = 0
                        freq[word] += 1
    # we sort our dictionary with respect to the frequence
    sortedfreq = sorted(freq.iteritems(), key=lambda (k, v): v, reverse=True)
    with open("b1.txt", "w") as f:
        # then we save rank, word and related frequence of the first 500 most frequent words on a file
        for i in range(500):
            f.write("%s %s %s\n" % ((i + 1), sortedfreq[i][1], sortedfreq[i][0]))
    return freq


if __name__ == '__main__':
    analyze("urls.txt")
